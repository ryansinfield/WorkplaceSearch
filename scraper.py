#!/usr/bin/python3

import json
import argparse
import datetime
from urllib.parse import quote
import time
import traceback
from WorkplaceSearch import *
from MariaDBConnect import *
import os

def main():
    # Globals
    global PAGE_SIZE
    global START_DATE
    global END_DATE
    global GRAPH_API_VERSION
    global CLEAR_CACHE
    GRAPH_API_VERSION="v2.11"

    # Add required argument for the search string
    parser = argparse.ArgumentParser()
    parser.add_argument("--token", "-t", help="Access token", required=True)
    parser.add_argument("--dbhost", "-host", help="The host name (or IP address) of the database server.", required=True)
    parser.add_argument("--dbuser", "-user", help="The username of the database user.", required=True)
    parser.add_argument("--dbpass", "-pass", help="The password of the database user.", required=True)
    parser.add_argument("--dbport", "-port", help="(Optional) The port number for the database instance.", default=3306, required=False)
    parser.add_argument("--start", "-s", help="(Optional) Specify a start date in format yyyy-mm-dd.", default=None, required=False)
    parser.add_argument("--end", "-e", help="(Optional) Specify an end date in format yyyy-mm-dd.", default=None, required=False)
    parser.add_argument("--pagesize","-ps", help="(Optional) Specify the number of results returned per page. Lower means the app will take longer, higher means there's more chance of a request timing out. (default 100)", \
                        default="100", required=False)
    parser.add_argument("--clearcache", "-cc", help="(Optional) Use this option to run the integration from scratch, clearing the previously cached data.", action="store_true")
    args = parser.parse_args()

    CLEAR_CACHE = args.clearcache

    # App token is passed as an argument so that it is not stored in the script
    APP_TOKEN = args.token
    PAGE_SIZE = int(args.pagesize)

    # Start and end date parameters
    if args.start is not None:
        try:
            START_DATE = int(datetime.datetime.strptime(args.start, "%Y-%m-%d").timestamp())
        except ValueError as ex:
            raise Exception("Specified start date not in format yyyy-mm-dd")
    else:
        START_DATE = None

    if args.end is not None:
        try:
            END_DATE = int(datetime.datetime.strptime(args.end, "%Y-%m-%d").timestamp())
        except ValueError as ex:
            raise Exception("Specified end date not in format yyyy-mm-dd")
    else:
        END_DATE = None

    if START_DATE is not None and END_DATE is not None and END_DATE < START_DATE:
        raise Exception("The start date can't come before the end date")

    # Get the database variables
    db_host = args.dbhost
    db_user = args.dbuser
    db_port = args.dbport
    db_pass = args.dbpass
    db_port = args.dbport

    print("Start time: %s\n" %datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))

    # Graph API object
    with RSinGraphAPI(GRAPH_API_VERSION, auth_token=APP_TOKEN, since=START_DATE, until=END_DATE) as fb:
        try:
            # Get all the data from Workplace in the correct format
            datafolder = "WorkplaceData"
            if (not os.path.isdir(datafolder)):
                os.mkdir(datafolder)
            wp_datafile = os.path.join(datafolder, "WP_Data.json")
            wp_data = update_workplace_data(fb, wp_datafile)

            # Print totals
            print("Total users: %s" %len(wp_data["Community_Members"]))
            print("Total groups: %s" %len(wp_data["Groups"]))
            print("Total events: %s" %len(wp_data["Group_Events"]))
            print("Total posts: %s" %len(wp_data["Posts"]))
            print("Total all_comments: %s" %len(wp_data["Comments"]))
            print("Total conversations: %s" %len(wp_data["Conversations"]))
            print("Total messages: %s" %len(wp_data["Messages"]))

            # Upload the data to the database
            table_defs = {
                "Community_Members":["member_id","name","first_name","last_name","department","link","updated_time","email"]
                ,"Groups":["group_id","updated_time","name","description","privacy","owner"]
                ,"Group_Events":["event_id","parent_group","name","description","updated_time"]
                ,"Posts":["post_id","parent_event","parent_group","created_time","from_member","message","picture","updated_time","link"]
                ,"Comments":["cmt_id","parent_post","message","parent_comment","from_member","created_time"]
                ,"Group_Members":["group_id","member_id","is_current"]
                ,"Conversations":["conversation_id","updated_time"]
                ,"Messages":["msg_id", "parent_conversation", "msg_text", "created_time", "from_member"]
                ,"Message_Recipients":["msg_id", "msg_sent_to"]
            }
            with MariaDBConnect(host=db_host, username=db_user, password=db_pass, port=db_port) as db:
                # Insert into each table separately
                for table_name,columns in table_defs.items():
                    print(table_name)
                    # Create a parameterised statement to insert a batch of records into each table
                    sql = db.compose_executemany_statement(table_name, columns)

                    # For each record in wp_data, find the column values
                    # If one or more is missing, just put null
                    params = []
                    for record in wp_data[table_name]:
                        attributes = {}
                        for column in columns:
                            if column == "from":
                                print("WTF!")
                            if column not in record:
                                attributes[column] = None
                            else:
                                attributes[column] = record[column]
                        params.append(attributes)

                    # Insert all table records into the database
                    db.insert_batch(sql, params)

        except Exception as ex:
            trace = traceback.format_exc()
            print("Fatal exception encountered.")
            print(trace)
            print("Exception time: %s" %datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))

def _attempt_fix_member_ids(entity_list, member_keys, member_ids):
    '''
    Attempt to convert any members in each entity in the entity_list to just their unique ID.

    :param entity_list: The list of entities that may contain user objects.
    :param member_keys: A list of keys where members may be present in the entities.
    :returns: The original entity list, having replaced the member objects with their unique ID.
    '''    
    fixed_list = entity_list
    for entity in entity_list:
        # Each record/entity
        for key in member_keys:
            # Each identified key
            if key in entity:
                # Key is present, is it a list of members or just one?
                if isinstance(entity[key], list):
                    for member in entity[key]:
                        member = _get_member_id(member, member_ids)
                else:
                    entity[key] = _get_member_id(entity[key], member_ids)
                
    return fixed_list

def _get_member_id(member, member_ids):
    try:
        # If it is not a member of the community, leave the field blank
        if member["id"] in member_ids:
            member = member["id"]
        else:
            print("%s is not a member of the community." %member["id"])
            member = None
    except KeyError as ex:
        print("No ID for member: %s." %member)
        pass
    return member

def _reformat_wp_data(wp_data):
    '''
    Reformat the dictionary returned from update_workplace_data to include the required fields

    :param wp_data: The dictionary from update_workplace_data
    :returns: A dictionary
    '''
    # Combine members and former_members to get the details of every Workplace account
    # Check for duplicates between former and current members and remove them as necessary
    wp_reformatted = {}
    c_mem_ids = [x["member_id"] for x in wp_data["community_members"]]
    community_former_members = [x for x in wp_data["community_former_members"] if x["member_id"] not in c_mem_ids]
    wp_reformatted["Community_Members"] = wp_data["community_members"] + community_former_members
    c_mem_ids = [x["member_id"] for x in wp_reformatted["Community_Members"]]

    # Add the community to the rest of the groups that were taken from Workplace
    all_groups = wp_data["groups"] + [wp_data["community"]]
    wp_reformatted["Groups"] = _attempt_fix_member_ids(all_groups, ["owner"], c_mem_ids)
    wp_reformatted["Group_Members"] = [x for x in wp_data["group_members"] if x in c_mem_ids]

    # Events
    events = list({v["event_id"]:v for v in wp_data["events"]}.values())

    # Remove duplicate events by converting to dictionary then back to list
    wp_reformatted["Group_Events"] = _attempt_fix_member_ids(events, ["owner"], c_mem_ids)

    # Get posts and comments
    wp_reformatted["Posts"] = _attempt_fix_member_ids(wp_data["posts"], ["from_member"], c_mem_ids)
    wp_reformatted["Comments"] = _attempt_fix_member_ids(wp_data["comments"], ["from_member"], c_mem_ids)

    # Conversations/messages
    # For conversations and messages, there is a possibility of duplicates;
    # to combat this, initially store them in a dictionary with the ID as the key before converting it to a list
    conversations = wp_data["conversations"]
    messages = wp_data["messages"]
    conv_dict = {}
    conv_id_map = {}
    for conversation in conversations:
        no_participants = len(conversation["participants"]["data"]) + len(conversation["former_participants"]["data"])
        old_c_id = conversation["conversation_id"]
        if no_participants == 2:
            # 1-1 conversation
            participant_ids = [x["id"] for x in conversation["participants"]["data"]]
            participant_ids.sort()
            c_id = "++".join(participant_ids)
        else:
            # Group conversation
            c_id = old_c_id.split("++")[0]

        # Compose and add the conversation to the dictionary if it isn't already present
        if c_id not in conv_dict:
            new_conv = {}
            new_conv["conversation_id"] = c_id
            new_conv["updated_time"] = conversation["updated_time"]
            conv_dict[c_id] = new_conv

        # Add the converted conversation Id to the lookup map
        conv_id_map[old_c_id] = c_id

    # Convert the conversation dictionary to a list
    wp_reformatted["Conversations"] = list(conv_dict.values())

    # Messages
    msg_dict = {}
    msg_recipients = []
    for msg in messages:
        parent_conversation = conv_id_map[msg["parent_conversation"]] if msg["parent_conversation"] in conv_id_map else msg["parent_conversation"]
        # Add the message if it doesn't exist
        if msg["msg_id"] not in msg_dict:
            new_msg = {}
            new_msg["parent_conversation"] = parent_conversation
            new_msg["msg_text"] = msg["msg_text"] 
            new_msg["created_time"] = msg["created_time"] 
            new_msg["from_member"] = msg["from_member"]["id"] if "from_member" in msg else ""
            new_msg["msg_id"] = msg["msg_id"]
            msg_dict[msg["msg_id"]] = new_msg

            # Add a record for each message participant
            if "to" in msg:
                for recipient in msg["to"]["data"]:
                    recip = {}
                    recip["msg_sent_to"] = recipient["id"]
                    recip["msg_id"] = msg["msg_id"]
                    msg_recipients.append(recip)
    
    # Convert the message dictionary to a list
    wp_reformatted["Message_Recipients"] = msg_recipients
    wp_reformatted["Messages"] = list(msg_dict.values())
    return wp_reformatted

def _get_cached_workplace_data(file_path):
    print("Using cached data from %s" %file_path)
    with open(file_path) as json_file:
        return json.load(json_file)
    print("Cached data loaded.")

def update_workplace_data(fb, savefile):
    '''
    Gets the data from Workplace using Facebook Graph API and writes that data to a file. If the file already exists,
    it just loads the data in that file into a dictionary and returns it.

    :param fb: An instance of the RSinGraphAPI class
    :param savefile: The destination path of the file
    :returns: A dictionary of Workplace data objects
    '''
    last_dir = savefile.rfind("\\")
    rawfile = savefile[:last_dir+1] + "RAW_" + savefile[last_dir+1:]

    # If the clearcache flag is set, delete the cached Workplace data files
    if CLEAR_CACHE:
        print("Deleting Workplace data cache files.")
        if os.path.isfile(savefile): os.remove(savefile)
        if os.path.isfile(rawfile): os.remove(rawfile)

    # If the data file already exists, load it into a dictionary and return it rather than querying facebook again
    # Try the processed data first, then try the raw data
    if os.path.isfile(savefile):
        return _get_cached_workplace_data(savefile)

    if os.path.isfile(rawfile):
        wp_data = _get_cached_workplace_data(rawfile)

    else:
        print("Querying data from Workplace.")

        # Community first
        wp_data = {}
        group_attributes = {
            "group_id": "id",
            "owner": "owner",
            "name": "name",
            "updated_time": "updated_time",
            "description": "description",
            "privacy": "privacy"
        }
        community = json.loads(fb.get("/community?fields=name,description,updated_time,privacy"))

        # Error in the community request
        if "error" in community:
            rsin_pprint(community["error"])
            raise Exception("Unable to get the community information.")

        wp_data["community"] = _find_attributes(group_attributes, community)
        print("Getting data from %s Workplace Community\n" %(community["name"]))

        # Members and former members
        member_attributes = {
            "member_id": "id",
            "name": "name",
            "first_name": "first_name",
            "last_name": "last_name",
            "department": "department",
            "link": "link",
            "updated_time": "updated_time",
            "email": "email",
            "picture": "picture"
        }
        community_members = get_objects(fb, community["id"], "members", member_attributes)
        former_community_members = get_objects(fb, community["id"], "former_members", member_attributes)

        wp_data["community_members"] = community_members
        wp_data["community_former_members"] = former_community_members
                
        # Groups
        groups = get_objects(fb, community["id"], "groups", group_attributes)
        wp_data["groups"] = groups

        # Group membership
        group_ids = [x["group_id"] for x in groups]
        group_members = get_batch_objects(fb, group_ids, "member", {"member_id":"id"}, "group")
        group_former_members = get_batch_objects(fb, group_ids, "former_member", {"member_id":"id"}, "group")

        # Add members and former members of groups to lists
        membership = []
        for member in group_members:
            membership.append({"member_id":member["member_id"], "group_id":member["parent_group"], "is_current":"1"})
        for f_member in group_former_members:
            membership.append({"member_id":f_member["member_id"], "group_id":f_member["parent_group"], "is_current":"0"})

        # Add community members to group membership table
        group_ids.append(community["id"])
        for member in community_members:
            membership.append({"member_id":member["member_id"], "group_id":community["id"], "is_current":"1"})
        for f_member in former_community_members:
            membership.append({"member_id":f_member["member_id"], "group_id":community["id"], "is_current":"0"})
            
        # Return the membership status of users
        wp_data["group_members"] = membership

        # Events in groups
        event_attributes = {
            "event_id": "id",
            "owner": "owner",
            "name": "name",
            "description": "description",
            "updated_time": "updated_time"
        }
        events = get_batch_objects(fb, group_ids, "event", event_attributes, "group")
        wp_data["events"] = events

        # Get all posts in groups
        post_attributes = {
            "post_id": "id",
            "created_time": "created_time",
            "message": "message",
            "picture": "picture",
            "updated_time": "updated_time",
            "link": "link",
            "from_member": "from"
        }
        group_posts = get_batch_objects(fb, group_ids, "post", post_attributes, "group")

        # Get all posts in events
        event_ids = [x["event_id"] for x in events]
        event_posts = get_batch_objects(fb, event_ids, "post", post_attributes, "event")

        # Combine group posts and event posts
        wp_data["posts"] = group_posts + event_posts

        # Comments and replies
        post_ids = [x["post_id"] for x in wp_data["posts"]]
        cmt_attributes = {
            "cmt_id": "id",
            "message": "message",
            "from_member": "from",
            "created_time": "created_time"
        }
        comments,replies = get_batch_objects(fb, post_ids, "comment", cmt_attributes, "post", "comment", cmt_attributes)
        all_comments = comments + replies
        wp_data["comments"] = all_comments

        # Conversations
        member_ids = [x["member_id"] for x in community_members]
        conversation_attributes = {
            "conversation_id": "id",
            "updated_time": "updated_time",
            "participants": "participants",
            "former_participants": "former_participants",
            "user": "user"
        }

        conversations = get_batch_objects(fb, member_ids, "conversation", conversation_attributes, user_reference=member_ids)
        wp_data["conversations"] = conversations

        # Messages
        conversation_ids,conversation_users = zip(*[(x["conversation_id"], x["user"]) for x in conversations])
        msg_attributes = {
            "msg_id": "id",
            "msg_text": "message",
            "created_time": "created_time",
            "from_member": "from",
            "to": "to"
        }
        messages = get_batch_objects(fb, conversation_ids, "message", msg_attributes, parent_type="conversation", user_reference=conversation_users)
        wp_data["messages"] = messages

        # Create a raw data file as well
        with open(rawfile, "w") as json_datafile:
            json.dump(wp_data, json_datafile)

    # Create the savefile and load the dictionary into it
    wp_data = _reformat_wp_data(wp_data)
    with open(savefile, "w") as wp_datafile:
        json.dump(wp_data, wp_datafile)

    # Return the data from Workplace
    return wp_data

def get_objects(fb, parent_id, edge, object_attributes):
    objects = []
    cursor = RSinGraphAPICursor(fb, edge, object_attributes.values(), parent_id, PAGE_SIZE)
    found_objects = cursor.execute_query()
    while True:
        # Add each object to the list to return
        for o_id, obj in found_objects.items():
            new_object = _find_attributes(object_attributes, obj)
            objects.append(new_object)

        # Check for next page of results
        if not cursor.has_next():
            break

        # Loop is still intact, get the next page of results
        found_objects = cursor.next()
    return objects

def get_batch_objects(fb, parent_ids, edge_type, object_attributes, parent_type=None, child_edge_type=None, child_attributes=None, user_reference=None):
    all_objects = []
    all_child_objects = []

    # Look up the edge name based on the type
    edge_map = {
        "post": "feed",
        "conversation": "conversations",
        "message": "messages",
        "event": "events",
        "member": "members",
        "group": "groups",
        "comment": "comments",
        "former_member": "former_members"
    }
    edge_name = edge_map[edge_type]
    child_edge_name = edge_map[child_edge_type] if child_edge_type is not None else None

    # Prepare the requests
    field_string = prepare_field_string(object_attributes.values())
    if child_edge_name is not None:
        # If there is a child object, append that to the field list 
        child_field_string = ",{0}.limit({1}){2}{3}{4}" \
            .format(child_edge_name, PAGE_SIZE, "{{%s}}" %prepare_field_string(child_attributes.values()), \
            ("since(%s)" %START_DATE) if START_DATE is not None else "", ("until(%s)" %END_DATE) if END_DATE is not None else "")
        field_string += child_field_string
    
    request_url = "{0}/{1}/{2}?fields={3}&limit={4}{5}".format(GRAPH_API_VERSION, "{0}", edge_name, field_string, PAGE_SIZE, "&user={1}" if user_reference is not None else "")

    # Get the request IDs
    request_ids = []
    for r_id in parent_ids:
        if "++" in r_id:
            # If the ++ separator exists, use the first section
            request_ids.append(r_id.split("++")[0])
        else:
            # If there is no separator, use the whole ID
            request_ids.append(r_id)

    print(request_url)
    requests = build_batch_request(request_ids, request_url, users=user_reference, since=START_DATE, until=END_DATE)

    # Create a cursor object to get results from Workplace
    cursor = RSinGraphAPIBatchCursor(fb, requests, object_attributes.values(), parent_ids, child_edge_name, child_attributes.values() if child_attributes is not None else None, user_reference=user_reference)
    results = cursor.execute_query()

    # Keep looping through the results until there are no more pages to return
    while True:
        for parent_id,objs in results.items():
            # Compose each object and add it to the return list
            for o_id,obj in objs.items():
                new_obj = _find_attributes(object_attributes, obj)
                if parent_type is not None:
                    new_obj["parent_%s" %parent_type] = parent_id
                all_objects.append(new_obj)

                # Handle children
                if child_edge_name is not None:
                    #if child_edge_name == "comments":
                    #    rsin_pprint(obj[child_edge_name])
                    if child_edge_name in obj:
                        for child_id,child_vals in obj[child_edge_name].items():
                            # Create the child object and add it to the child list
                            child = _find_attributes(child_attributes, child_vals)
                            #print(child["message"])
                            child["parent_%s" %edge_type] = obj["id"]
                            if parent_type is not None:
                                child["parent_%s" %parent_type] = parent_id
                            all_child_objects.append(child)
                            
        # If there are no more pages, break the loop
        if not cursor.has_next():
            break

        # Get the next page of results
        results = cursor.next()

    if child_edge_name is not None:
        return all_objects,all_child_objects

    return all_objects

def _find_attributes(attribute_map, source_object):
    to_return = {}
    for dst,src in attribute_map.items():
        # Dot-notation support
        src_fields = src.split(".")
        try:
            field_value = source_object[src_fields[0]]
            i=1
            while i<len(src_fields):
                field_value = field_value[src_fields[i]]
                i+=1

            # Try to format the Workplace datetime
            if isinstance(field_value, str):
                try:
                    field_value = _strip_workplace_timestamp(field_value)
                except ValueError:
                    pass
                
            # Add the ultimate field to the return dictionary as the destination key
            to_return[dst] = field_value
        except KeyError as ex:
            #print(str(ex))
            pass
    return to_return

def _strip_workplace_timestamp(wp_time):
    return datetime.datetime.strptime(wp_time,"%Y-%m-%dT%H:%M:%S+0000").strftime("%Y-%m-%d %H:%M")

# Run
if __name__ == "__main__":
    main()