-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema WorkplaceData
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema WorkplaceData
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `WorkplaceData` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ;
USE `WorkplaceData` ;

-- -----------------------------------------------------
-- Table `WorkplaceData`.`Community_Members`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `WorkplaceData`.`Community_Members` ;

CREATE TABLE IF NOT EXISTS `WorkplaceData`.`Community_Members` (
  `member_id` CHAR(50) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `first_name` VARCHAR(255) NOT NULL,
  `last_name` VARCHAR(255) NOT NULL,
  `department` TEXT NULL,
  `link` TEXT NOT NULL,
  `updated_time` DATETIME NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `picture` TEXT NULL,
  PRIMARY KEY (`member_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `WorkplaceData`.`Groups`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `WorkplaceData`.`Groups` ;

CREATE TABLE IF NOT EXISTS `WorkplaceData`.`Groups` (
  `group_id` CHAR(50) NOT NULL,
  `owner` CHAR(50) NULL,
  `updated_time` DATETIME NOT NULL,
  `name` TEXT NOT NULL,
  `description` TEXT NULL,
  `privacy` ENUM('CLOSED', 'OPEN', 'SECRET') NOT NULL,
  PRIMARY KEY (`group_id`),
  INDEX `fk_Groups_Members1_idx` (`owner` ASC),
  CONSTRAINT `fk_Groups_Members1`
    FOREIGN KEY (`owner`)
    REFERENCES `WorkplaceData`.`Community_Members` (`member_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `WorkplaceData`.`Group_Events`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `WorkplaceData`.`Group_Events` ;

CREATE TABLE IF NOT EXISTS `WorkplaceData`.`Group_Events` (
  `event_id` CHAR(50) NOT NULL,
  `owner` CHAR(50) NULL,
  `parent_group` CHAR(50) NOT NULL,
  `name` TEXT NOT NULL,
  `description` TEXT NULL,
  `updated_time` DATETIME NOT NULL,
  PRIMARY KEY (`event_id`),
  INDEX `fk_Events_Members_idx` (`owner` ASC),
  INDEX `fk_Events_Groups1_idx` (`parent_group` ASC),
  CONSTRAINT `fk_Events_Members`
    FOREIGN KEY (`owner`)
    REFERENCES `WorkplaceData`.`Community_Members` (`member_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Events_Groups1`
    FOREIGN KEY (`parent_group`)
    REFERENCES `WorkplaceData`.`Groups` (`group_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `WorkplaceData`.`Posts`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `WorkplaceData`.`Posts` ;

CREATE TABLE IF NOT EXISTS `WorkplaceData`.`Posts` (
  `post_id` CHAR(50) NOT NULL,
  `parent_event` CHAR(50) NULL,
  `parent_group` CHAR(50) NULL,
  `created_time` DATETIME NOT NULL,
  `from_member` CHAR(50) NULL,
  `message` TEXT NULL,
  `picture` TEXT NULL,
  `updated_time` DATETIME NOT NULL,
  `link` TEXT NULL,
  PRIMARY KEY (`post_id`),
  INDEX `fk_Posts_Events1_idx` (`parent_event` ASC),
  INDEX `fk_Posts_Groups1_idx` (`parent_group` ASC),
  INDEX `fk_Posts_Members1_idx` (`from_member` ASC),
  CONSTRAINT `fk_Posts_Events1`
    FOREIGN KEY (`parent_event`)
    REFERENCES `WorkplaceData`.`Group_Events` (`event_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Posts_Groups1`
    FOREIGN KEY (`parent_group`)
    REFERENCES `WorkplaceData`.`Groups` (`group_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Posts_Members1`
    FOREIGN KEY (`from_member`)
    REFERENCES `WorkplaceData`.`Community_Members` (`member_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `WorkplaceData`.`Comments`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `WorkplaceData`.`Comments` ;

CREATE TABLE IF NOT EXISTS `WorkplaceData`.`Comments` (
  `cmt_id` CHAR(50) NOT NULL,
  `parent_post` CHAR(50) NOT NULL,
  `message` TEXT NULL,
  `parent_comment` CHAR(50) NULL,
  `from_member` CHAR(50) NULL,
  `created_time` DATETIME NOT NULL,
  PRIMARY KEY (`cmt_id`),
  INDEX `fk_Comments_Posts1_idx` (`parent_post` ASC),
  INDEX `fk_Comments_Comments1_idx` (`parent_comment` ASC),
  INDEX `fk_Comments_Members1_idx` (`from_member` ASC),
  CONSTRAINT `fk_Comments_Posts1`
    FOREIGN KEY (`parent_post`)
    REFERENCES `WorkplaceData`.`Posts` (`post_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Comments_Comments1`
    FOREIGN KEY (`parent_comment`)
    REFERENCES `WorkplaceData`.`Comments` (`cmt_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Comments_Members1`
    FOREIGN KEY (`from_member`)
    REFERENCES `WorkplaceData`.`Community_Members` (`member_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `WorkplaceData`.`Group_Members`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `WorkplaceData`.`Group_Members` ;

CREATE TABLE IF NOT EXISTS `WorkplaceData`.`Group_Members` (
  `group_id` CHAR(50) NOT NULL,
  `member_id` CHAR(50) NOT NULL,
  `is_current` TINYINT NOT NULL,
  INDEX `fk_Group_Members_Members1_idx` (`member_id` ASC),
  PRIMARY KEY (`group_id`, `is_current`, `member_id`),
  CONSTRAINT `fk_Group_Members_Groups1`
    FOREIGN KEY (`group_id`)
    REFERENCES `WorkplaceData`.`Groups` (`group_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Group_Members_Members1`
    FOREIGN KEY (`member_id`)
    REFERENCES `WorkplaceData`.`Community_Members` (`member_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `WorkplaceData`.`Conversations`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `WorkplaceData`.`Conversations` ;

CREATE TABLE IF NOT EXISTS `WorkplaceData`.`Conversations` (
  `conversation_id` CHAR(50) NOT NULL,
  `updated_time` DATETIME NOT NULL,
  PRIMARY KEY (`conversation_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `WorkplaceData`.`Messages`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `WorkplaceData`.`Messages` ;

CREATE TABLE IF NOT EXISTS `WorkplaceData`.`Messages` (
  `msg_id` CHAR(50) NOT NULL,
  `parent_conversation` CHAR(50) NOT NULL,
  `msg_text` TEXT NULL,
  `created_time` DATETIME NOT NULL,
  `from_member` CHAR(50) NULL,
  PRIMARY KEY (`msg_id`),
  INDEX `fk_Messages_Conversations1_idx` (`parent_conversation` ASC),
  CONSTRAINT `fk_Messages_Conversations1`
    FOREIGN KEY (`parent_conversation`)
    REFERENCES `WorkplaceData`.`Conversations` (`conversation_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `WorkplaceData`.`Message_Recipients`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `WorkplaceData`.`Message_Recipients` ;

CREATE TABLE IF NOT EXISTS `WorkplaceData`.`Message_Recipients` (
  `msg_id` CHAR(50) NOT NULL,
  `msg_sent_to` CHAR(50) NOT NULL,
  PRIMARY KEY (`msg_id`, `msg_sent_to`),
  CONSTRAINT `fk_Message_Recipients_Messages1`
    FOREIGN KEY (`msg_id`)
    REFERENCES `WorkplaceData`.`Messages` (`msg_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `WorkplaceData`.`Message_Attachments`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `WorkplaceData`.`Message_Attachments` ;

CREATE TABLE IF NOT EXISTS `WorkplaceData`.`Message_Attachments` (
  `attachment_id` CHAR(50) NOT NULL,
  `parent_message` CHAR(50) NOT NULL,
  `mime_type` VARCHAR(45) NULL,
  `name` INT NULL,
  `size` INT NULL,
  PRIMARY KEY (`attachment_id`),
  INDEX `fk_Message_Attachments_Messages1_idx` (`parent_message` ASC),
  CONSTRAINT `fk_Message_Attachments_Messages1`
    FOREIGN KEY (`parent_message`)
    REFERENCES `WorkplaceData`.`Messages` (`msg_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `WorkplaceData`.`Post_Attachments`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `WorkplaceData`.`Post_Attachments` ;

CREATE TABLE IF NOT EXISTS `WorkplaceData`.`Post_Attachments` (
  `attachment_id` CHAR(50) NOT NULL,
  `parent_post` CHAR(50) NOT NULL,
  `mime_type` VARCHAR(45) NULL,
  `name` VARCHAR(45) NULL,
  `size` INT NULL,
  PRIMARY KEY (`attachment_id`),
  INDEX `fk_Post_Attachments_Posts1_idx` (`parent_post` ASC),
  CONSTRAINT `fk_Post_Attachments_Posts1`
    FOREIGN KEY (`parent_post`)
    REFERENCES `WorkplaceData`.`Posts` (`post_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
