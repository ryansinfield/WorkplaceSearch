import http.client
import json
from urllib.parse import quote
import time
import datetime

class RSinGraphAPI:
    def __init__(self, api_version, auth_token=None, host="graph.facebook.com", since=None, until=None):
        if auth_token is None: raise Exception("You need to specify an access token.")
        self.auth_token = auth_token
        self.host = host
        self.connection = http.client.HTTPSConnection(self.host, 443)
        self.api_version = api_version
        self.since = since
        self.until = until

    def __enter__(self):
        return self

    def __exit__(self, type, value, tb):
        self.connection.close()

    def set_fields(self, fields):
        self.fields = fields

    def set_parms(self, parms):
        self.parms = parms

    def get(self, target, params=None, token=None):
        token = self.auth_token if token is None else token
        headers = {"Authorization": "Bearer %s" %(token), "Accept-Encoding":"utf-8"}
        self.connection.request("GET", target, headers=headers)
        res = self.connection.getresponse()
        txt = res.read().decode("utf-8")
        return txt

    def get_batch(self, batch, token=None):
        token = self.auth_token if token is None else token

        # Convert the target dictionary to a JSON array string
        batch = quote(json.dumps(batch))
        headers = {"Accept-Encoding":"utf-8"}
        target = "/%s/?batch=%s&access_token=%s&include_headers=false" %(self.api_version, batch.replace(" ",""), token)

        self.connection.request("POST", target, headers=headers)
        res = self.connection.getresponse()
        txt = res.read().decode("utf-8")
        try:
            json_res = json.loads(txt)
        except ValueError as ex:
            print(txt)
            raise Exception("Could not decode response from Facebook.")
        return json_res

    @staticmethod
    def inject_api_version(url, api_version, since=None, until=None):
        url = "/" + api_version + url[url.find("/",27):]
        if since is not None: url = url + "&since=%s" %since
        if until is not None: url = url + "&until=%s" %until
        return url

class RSinGraphAPIBatchCursor:
    def __init__(self, fb, request_batch, object_attributes, parent_reference=None, child_object=None, child_object_attributes=None, access_token=None, user_reference=None, batch_size=50):
        self.fb = fb
        self.request_batch = request_batch
        self.object_attributes = object_attributes
        self.access_token = access_token
        self.user_reference = user_reference
        self.parent_reference = parent_reference
        self.batch_size = batch_size
        self.child_object = child_object
        self.child_object_attributes = child_object_attributes

        # Keep track of users and parents
        self.next_batch = []
        self.next_users = []
        self.next_parents = []

    def _get_from_fb(self):
        # Each batch request can't exceed the maximum, so split the whole request into chunks
        print("Request batch length: %s" %len(self.request_batch))
        batches = portion(self.request_batch, self.batch_size)
        parent_batches = portion(self.parent_reference if self.parent_reference is not None else [], self.batch_size)
        user_batches = portion(self.user_reference if self.user_reference is not None else [], self.batch_size)
        print("Total portions in batch: %s" %len(batches))
        to_return = {}

        # Send each batch request
        for i,batch in enumerate(batches):
            # Get the current user and parent batches
            if len(user_batches) > 0:
                user_batch = user_batches[i]
            else:
                user_batch = None
            if len(parent_batches) > 0:
                parent_batch = parent_batches[i]
            else:
                parent_batch = None

            req_start = time.time()
            responses = self.fb.get_batch(batch, self.access_token)
            print("Time taken for response: %s" %(time.time() - req_start))
            print("Response length: %s" %len(responses))
            
            # Check that the number of responses matches the total number of references
            if user_batch is not None and len(user_batch) != len(responses):
                rsin_pprint(responses)
                raise Exception("The number of responses doesn't match the number of users in the reference list.")
            if parent_batch is not None and len(parent_batch) != len(responses):
                rsin_pprint(responses)
                raise Exception("The number of responses doesn't match the number of parent IDs in the reference list. Parent IDs: %s, Responses: %s" %(len(parent_batch), len(responses)))

            # Handle errors for the whole response
            if "error" in responses:
                rsin_pprint(responses)
                raise Exception("There was a problem with the response. Facebook returned an error message for the whole batch after %s seconds." %(time.time() - req_start))

            for n,res in enumerate(responses):
                # Keep track of the parent ID and user (if applicable) of each response
                if user_batch is not None:
                    this_user = user_batch[n]
                else:
                    this_user = None
                if parent_batch is not None:
                    parent_id = parent_batch[n]
                else:
                    parent_id = None

                # Load the JSON response into a dictionary
                body = json.loads(res["body"])

                # Handle an error on this response
                if "error" in body:
                    '''
                    # Error code 100/subcode 33
                    # Object with ID does not exist or cannot be loaded due to missing permissions.
                    # Ignore this error and continue to the next response
                    if "code" in body["error"] and "error_subcode" in body["error"]:
                        if body["error"]["code"] == 100 and body["error"]["error_subcode"] == 33:
                            print(parent_id)
                            print("The request URL below failed to return a result. Either the object doesn't exist or it cannot be loaded due to missing permissions.")
                            print(batch[n]["relative_url"])
                            print("Continuing")
                            continue
                    '''
                    print(batch[n])
                    rsin_pprint(body)
                    raise Exception("There was a problem with the response. The parent was '%s'. Facebook returned an error message." %parent_id)

                # Load the data key if it exists
                try:
                    data = body["data"]
                except KeyError as ex:
                    rsin_pprint(body)
                    raise Exception("The response was missing the 'data' key.")

                # If the parent_id is a list, then the objects are children and need to be treated differently
                if isinstance(parent_id, list):
                    for obj in data:
                        o_id = obj["id"]
                        # Get the children that were included with the response, add them to the batch request and parent/user references if applicable
                        gparent_id = parent_id[0]
                        parent_id = parent_id[1]
                        obj_data = {}

                        # Get all the requested child attributes
                        for attr in self.child_object_attributes:
                            if "." in attr:
                                attr = attr[:attr.find(".")]
                            if attr in obj:
                                obj_data[attr] = obj[attr]

                        # If the gparent/parent keys don't exist in the dictionary, they'll need to be created
                        if gparent_id not in to_return:
                            to_return[gparent_id] = {}
                        if parent_id not in to_return[gparent_id]:
                            to_return[gparent_id][parent_id] = {}
                        if self.child_object not in to_return[gparent_id][parent_id]:
                            to_return[gparent_id][parent_id][self.child_object] = {}

                        # Add the child object to its parent in the return dictionary
                        to_return[gparent_id][parent_id][self.child_object][o_id] = obj_data
                        #rsin_pprint(to_return)

                    # Add the next page to the batch request
                    try:
                        url = RSinGraphAPI.inject_api_version(body["paging"]["next"], self.fb.api_version, self.fb.since, self.fb.until)
                        self.next_batch.append({"method":"GET", "relative_url": url})
                        # Update the parent/user references as appropriate
                        ref = [gparent_id, parent_id]
                        self.next_parents.append(ref)
                        if self.user_reference is not None:
                            self.next_users.append(this_user)
                    except KeyError as ex:
                        pass

                else:
                    for obj in data:
                        # If the object is a conversation, generate a unique ID based on the chat ID and user
                        if self.user_reference is not None and "participants" in obj:
                            o_id = obj["id"] + "++" + this_user
                        else:
                            o_id = obj["id"]

                        # Include the requested attributes
                        obj_data = {}
                        for attr in self.object_attributes:
                            if attr in obj:
                                obj_data[attr] = obj[attr]
                        obj_data["id"] = o_id

                        # Check if the response contains any of the specified children
                        if self.child_object is not None:
                            obj_data[self.child_object] = {}
                            if self.child_object in obj:
                                for child in obj[self.child_object]["data"]:
                                    child_id = child["id"]
                                    if child_id not in obj_data[self.child_object]:
                                        ch = {}
                                        for child_attr in self.child_object_attributes:
                                            if child_attr in child:
                                                ch[child_attr] = child[child_attr]
                                        obj_data[self.child_object][child_id] = ch
                                
                                # If it exists, add the link for the next page to the batch request
                                try:
                                    url = RSinGraphAPI.inject_api_version(obj[self.child_object]["paging"]["next"], self.fb.api_version, self.fb.since, self.fb.until)
                                    self.next_batch.append({"method":"GET", "relative_url":url})
                                    # Add a list reference instead of string because the child will be nested in the dictionary
                                    ref = [parent_id, o_id]
                                    self.next_parents.append(ref)
                                    if self.user_reference is not None:
                                        self.next_users.append(this_user)
                                except KeyError as ex:
                                    pass

                        # Check if a user reference was included. If so, add it to the object data.
                        if self.user_reference is not None:
                            obj_data["user"] = this_user
        
                        # If a parent_reference was included, use the parent ID as the root-level key
                        if self.parent_reference is not None:
                            if parent_id not in to_return:
                                to_return[parent_id] = {}
                            if o_id not in to_return[parent_id]:
                                to_return[parent_id][o_id] = obj_data
                        # Otherwise, use the object ID as the root-level key
                        else:
                            if o_id not in to_return:
                                to_return[o_id] = obj_data

                    # Add the next page URL into the next batch request
                    try:
                        url = RSinGraphAPI.inject_api_version(body["paging"]["next"], self.fb.api_version, self.fb.since, self.fb.until)
                        self.next_batch.append({"method":"GET", "relative_url":url})
                        # Update the parent/user references as appropriate
                        # This section won't run if there is no paging/next key          
                        if self.parent_reference is not None:
                            self.next_parents.append(parent_id)
                        if self.user_reference is not None:
                            self.next_users.append(this_user)
                    except KeyError as ex:
                        pass    # Ignore KeyErrors here, it just means there's not another page of results.

        # Move the next_* variables into the live ones
        self.request_batch = self.next_batch
        self.parent_reference = self.next_parents if len(self.next_parents) > 0 else None
        self.user_reference = self.next_users if len(self.next_users) > 0 else None
        self.next_batch = []
        self.next_parents = []
        self.next_users = []

        # Return the objects
        #rsin_pprint(to_return)
        return to_return

    def execute_query(self):
        return self._get_from_fb()

    def has_next(self):
        return len(self.request_batch) > 0

    def next(self):
        return self._get_from_fb()

class RSinGraphAPICursor:
    def __init__(self, fb, edge_type, object_attributes, parent_id, limit, access_token=None, since=None, until=None):
        self.fb = fb
        self.edge_type = edge_type
        self.object_attributes = object_attributes
        self.parent_id = parent_id
        self.access_token = access_token
        self.since = since
        self.until = until
        self.request_url = self._build_url(edge_type, object_attributes, self.fb.api_version, parent_id, limit, access_token, since, until)

    def _build_url(self, edge_type, object_attributes, api_version, parent_id, limit, access_token, since, until):
        # Return the URL for the request
        fields = prepare_field_string(object_attributes, 1)
        url = "/{0}/{1}/{2}?fields={3}&limit={4}".format(api_version, parent_id, edge_type, fields, limit)
        
        # Optional parts of the URL
        if access_token is not None:
            url = url + "&access_token=" + access_token
        if since is not None:
            url = url + "&since=" + since
        if until is not None:
            url = url + "&until=" + until

        return url
        
    def _get_from_fb(self, fb, request_url, object_attributes, access_token):
        response = fb.get(request_url, access_token)
        try:
            self.current_page = json.loads(response)
        except ValueError as ex:
            print(response)
            raise Exception("Could not decode response from Facebook.")

        # Check for errors
        if "error" in self.current_page:
            rsin_pprint(self.current_page)
            raise Exception("There was a problem getting this page of results. Facebook sent back an error message.")

        # Return the requested attributes
        results = {}
        for obj in self.current_page["data"]:
            result = {}
            for attr in object_attributes:
                if attr in obj:
                    result[attr] = obj[attr]
            # Add to the dictionary of results
            results[obj["id"]] = result

        return results
    
    def execute_query(self):
        # Send the first request to Facebook
        print(self.request_url)
        return self._get_from_fb(self.fb, self.request_url, self.object_attributes, self.access_token)

    def has_next(self):
        if self.current_page is not None:
            if "paging" in self.current_page and "next" in self.current_page["paging"]:
                return True
            else:
                return False
        else:
            raise Exception("There is no current page, how could there be a next page?!")

    def next(self):
        self.request_url = RSinGraphAPI.inject_api_version(self.current_page["paging"]["next"], self.fb.api_version, self.fb.since, self.fb.until)
        return self._get_from_fb(self.fb, self.request_url, self.object_attributes, self.access_token)

    def has_last(self):
        if self.current_page is not None:
            if "paging" in self.current_page and "last" in self.current_page["paging"]:
                return True
            else:
                return False
        else:
            raise Exception("There is no current page, how could there be a last page?!")

    def last(self, fb, object_attributes, access_token):
        self.request_url = RSinGraphAPI.inject_api_version(self.current_page["paging"]["last"], self.fb.api_version, self.fb.since, self.fb.until)
        return _get_from_fb(fb, self.request_url, object_attributes, access_token)

class RSinWSModelBatchListInsert:
    def __init__(self, table_name, insert_list):
        self.insert_list = insert_list
        self.table_name = table_name

    def generate_sql(self):
        stmts = []
        # Loop through every object in object_list and generate an SQL statement
        for obj in self.insert_list:
            sql = "INSERT INTO {0} ({1}) VALUES ({2});".format(self.table_name, ",".join(list(obj.keys())), ",".join(list(obj.values())))
            stmts.append(sql)

        # Return a list of SQL statements
        return stmts

def rsin_pprint(json_str):
    print(json.dumps(json_str, sort_keys=False, indent=2, separators=(", ", ": ")))

def build_batch_request(parent_ids, url, users=None, since=None, until=None):
    requests = []
    for n,p_id in enumerate(parent_ids):
        # Add the parent ID to the request
        # Also add the user attribute if it's present
        if users is not None:
            request_url = url.format(p_id, users[n])
        else:
            request_url = url.format(p_id)

        # Add the pre-defined start and end dates
        if since is not None:
            request_url += "&since=%s" %since
        if until is not None:
            request_url += "&until=%s" %until

        # Add the request to the batch
        requests.append({"method":"GET", "relative_url":request_url})
    return requests

def portion(pie, portion_size, only_full_sets=False):
    # Split an array
    portions = []
    i = 0
    while i < (len(pie) / portion_size):
        floor = i * portion_size
        ceil = (i+1) * portion_size if (i+1) * portion_size <= len(pie) else len(pie)
        if only_full_sets and ceil - floor < portion_size: break
        portion = pie[floor:ceil]
        portions.append(portion)
        i+=1
    return portions

def prepare_field_string(attributes, brace_depth=2):
    field_string = ""
    for n,attr in enumerate(attributes):
        if n>0:
            field_string += ","

        if "." in attr:
            attrs = attr.split(".")
            attr = attrs[0]
            attr = attrs[0] + ("{" * brace_depth)
            i=1
            while i<len(attrs):
                if i>1:
                    attr += ","
                
                attr += attrs[i]
                i+=1
            attr += ("}" * brace_depth)
        field_string += attr 
    return field_string