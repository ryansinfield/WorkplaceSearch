import pymysql.cursors

class MariaDBConnect:
    def __init__(self, host, username, password, dbname="WorkplaceData", port=3306, secure=True, charset="utf8mb4"):
        self.host = host
        self.username = username
        self.password = password
        self.dbname = dbname
        self.port = port
        if secure == True:
            ssl = {"ca":None}
        else:
            ssl = None
        self.connection = pymysql.connect(host=self.host, user=self.username, password=self.password, db=self.dbname, 
            cursorclass=pymysql.cursors.DictCursor, port=self.port, ssl=ssl, charset=charset)
            
        with self.connection.cursor() as cursor:
            cursor.execute("SET NAMES %s" %charset)
    
    def __enter__(self):
        return self

    def __exit__(self, type, value, tb):
        self.connection.close()

    def _compose_insert(self, table, values):
        # Manufacture the SQL insert statement
        column_list = ",".join(map(str, values.keys()))
        params = list(map(str, values.values()))
        sql = "INSERT INTO {0} ({1}) VALUES ({2});".format(table, column_list, ",".join(["%s" for x in params]))
        return sql, params

    def compose_executemany_statement(self, table, columns):
        # Compose a SQL statement for use in the executemany function
        column_list = ",".join(map(str, columns))
        sql = "INSERT INTO {0} ({1}) VALUES ({2})".format(table, column_list, ",".join(["%({0})s".format(x) for x in columns]))
        return sql

    def insert_record(self, table, values):
        '''
        Inserts a record into a table

        :param table: The name of the table into which you want to insert a record
        :param values: A dictionary containing the values you want to insert. The dictionary keys must match the column names
        '''
        # Manufacture the SQL insert statement
        sql, params = self._compose_insert(table, values)

        # Create a cursor and commit the transaction
        with self.connection.cursor() as cursor:
            cursor.execute(sql, params)
        
        self.connection.commit()

    def insert_batch(self, sql, params):
        '''
        Inserts a batch of records into the database.

        :param sql: A parameterised SQL INSERT statement
        :param params: A dictionary of parameters to pass to the executemany function
        '''
        # Create a cursor and call executemany()
        with self.connection.cursor() as cursor:
            try:
                cursor.executemany(sql, params)
            except pymysql.Error as ex:
                f = open("pymysql.error","w",encoding="utf-8")
                f.write(str(ex))
                f.write("\n\n")
                for param in params:
                    f.write(str(param) + "\n")
                raise Exception("pymysql.Error: " + str(ex))

        # Commit the transaction
        self.connection.commit()